export default class Table {
  constructor(schema) {
    this.data = new Map()
    this.schema = schema
  }

  insert(record) {
    this.schema.validateChangeset(record, this)
    this.data.set(record.id, record)
  }

  getRecord(id) {
    return this.data.get(id)
  }

  valueMatchesQuery(queriedValue, subQuery) {
    const operations = {
      $gt: (check, value) => value > check,
      $lt: (check, value) => value < check,
      $eq: (check, value) => value === check,
    }
    const [operationName, value] = Object.entries(subQuery)[0]
    const operation = operations[operationName]
    return operation(value, queriedValue)
  }

  recordMatchesQuery(record, query) {
    for (const key of Object.keys(query)) {
      const queriedValue = record[key]
      const subQuery = query[key]
      if (!this.valueMatchesQuery(queriedValue, subQuery)) {
        return false
      }
    }
    return true
  }

  query(query) {
    return Array.from(this.data.values()).filter((record) =>
      this.recordMatchesQuery(record, query),
    )
  }

  update(query, changeset) {
    this.query(query).map((record) => Object.assign(record, changeset))
  }

  delete(query) {
    this.query(query).forEach((record) => this.data.delete(record.id))
  }
}

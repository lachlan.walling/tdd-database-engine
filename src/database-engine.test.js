import DatabaseEngine from './database-engine'
import { Check, Schema, Unique } from './schema'

/*
  TODO
  multiple tables
  schema validation
  check key is in schema
  type and other checks on insert
 */

describe('database-engine', () => {
  let databaseEngine, users
  beforeEach(() => {
    databaseEngine = new DatabaseEngine()
    databaseEngine.createTable(
      'users',
      new Schema({
        id: [Number, Unique],
        name: String,
        age: [Number, new Check((age) => age > 0)],
      }),
    )
    users = databaseEngine.getTable('users')
  })

  it('creates tables', () => {
    expect(users !== undefined)
  })

  it('inserts a record', () => {
    const record = { id: 1, name: 'Alice', age: 30 }
    users.insert(record)
    const retrievedRecord = users.getRecord(1)

    expect(retrievedRecord).toEqual(record)
  })

  describe('queries records', () => {
    let user1
    let user2
    beforeEach(() => {
      user1 = { id: 1, name: 'Alice', age: 30 }
      user2 = { id: 2, name: 'Bob', age: 25 }
      users.insert(user1)
      users.insert(user2)
    })

    describe('given a query with a single property', () => {
      it('queries the property is greater than a number', () => {
        expect(users).toBeDefined()
        const results = users.query({ age: { $gt: 28 } })
        expect(results).toEqual([user1])
      })

      it('queries the property is less than a number', () => {
        const results = users.query({ age: { $lt: 28 } })
        expect(results).toEqual([user2])
      })

      it('queries the property is equal to a number', () => {
        const results = users.query({ age: { $eq: 30 } })
        expect(results).toEqual([user1])
      })
    })
    describe('given a query with multiple properties', () => {
      it('returns no matches if none of the properties match', () => {
        const results = users.query({
          age: { $eq: 31 },
          name: { $eq: 'missing' },
        })
        expect(results).toEqual([])
      })
      it('returns no matches if only one of the properties match', () => {
        const results = users.query({
          age: { $eq: 30 },
          name: { $eq: 'missing' },
        })
        expect(results).toEqual([])
      })
      it('returns a match if both properties match', () => {
        const results = users.query({
          age: { $eq: 30 },
          name: { $eq: 'Alice' },
        })
        expect(results).toEqual([user1])
      })
    })
  })

  describe('updates records', () => {
    it('updates a single record', () => {
      users.insert({ id: 1, name: 'Alice', age: 30 })
      users.update({ id: { $eq: 1 } }, { name: 'Alicia' })
      const updatedRecord = users.getRecord(1)
      expect(updatedRecord.name).toBe('Alicia')
    })

    it('Updates multiple records', () => {
      users.insert({ id: 1, name: 'Alice', age: 30 })
      users.insert({ id: 2, name: 'Bob', age: 25 })
      users.update({ age: { $gt: 20 } }, { age: 40 })
      const results = users.query({ age: { $eq: 40 } })
      expect(results.length).toBe(2)
    })
  })

  describe('deletes records', () => {
    it('Deletes a single record', () => {
      users.insert({ id: 1, name: 'Alice', age: 30 })
      users.delete({ id: { $eq: 1 } })
      const deletedRecord = users.getRecord(1)
      expect(deletedRecord).toBe(undefined)
    })

    it('deletes multiple records', () => {
      users.insert({ id: 1, name: 'Alice', age: 30 })
      users.insert({ id: 2, name: 'Bob', age: 25 })
      users.delete({ age: { $lt: 31 } })
      const results = users.query({ age: { $lt: 31 } })
      expect(results.length).toBe(0)
    })
  })

  describe('validation', () => {
    it('type checks', () => {
      expect(() =>
        users.insert({ id: 'one', name: 'Alice', age: 30 }),
      ).toThrow()
    })
    it('unique', () => {
      users.insert({ id: 1, name: 'Alice', age: 30 })
      expect(() => {
        users.insert({ id: 1, name: 'Bob', age: 25 })
      }).toThrow()
    })
    it('check function', () => {
      expect(() => users.insert({ id: 1, name: 'Alice', age: -5 })).toThrow()
    })
  })
})

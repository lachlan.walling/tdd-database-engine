import Table from './table'

class DatabaseEngine {
  constructor() {
    this.tables = new Map()
  }

  createTable(name, schema) {
    this.tables.set(name, new Table(schema))
  }

  getTable(name) {
    return this.tables.get(name)
  }
}

module.exports = DatabaseEngine

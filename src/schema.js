class SchemaValidator {
  validateProperty(key, value) {}
}

class TypeValidator extends SchemaValidator {
  constructor(type) {
    super()
    this.type = type
  }
  validateProperty(key, value) {
    if (typeof value !== this.type)
      throw new Error(
        `Invalid value ${value} of type ${this.type} for property ${key}`,
      )
  }
}

export class Unique extends SchemaValidator {
  validateProperty(key, value, table) {
    if (table.query({ [key]: { $eq: value } }).length !== 0)
      throw new Error(
        `Value ${value} for property ${key} violates unique constraint`,
      )
  }
}

export class Check extends SchemaValidator {
  constructor(checkFunction) {
    super()
    this.checkFunction = checkFunction
  }

  validateProperty(key, value, table) {
    if (!this.checkFunction(value))
      throw new Error(
        `Value ${value} for property ${key} violates check function`,
      )
  }
}

const validators = {
  [String]: new TypeValidator('string'),
  [Number]: new TypeValidator('number'),
  [Unique]: new Unique(),
}

export class Schema {
  constructor(spec) {
    this.spec = spec
  }

  resolveIndividualValidator(validatorSpec) {
    if (validatorSpec instanceof SchemaValidator) {
      return validatorSpec
    } else return validators[validatorSpec]
  }

  resolveValidators(validatorSpec) {
    if (validatorSpec instanceof Array)
      return validatorSpec.map(this.resolveIndividualValidator)
    else return [this.resolveIndividualValidator(validatorSpec)]
  }

  validateChangeset(changeset, table) {
    Object.entries(changeset).forEach(([key, value]) => {
      const validatorSpec = this.spec[key]
      this.resolveValidators(validatorSpec).forEach((validator) =>
        validator.validateProperty(key, value, table),
      )
    })
  }
}
